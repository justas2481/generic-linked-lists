# Generic Linked Lists

Purpose of this project was to familiarize myself with Go's new feature - generics. There are 2 types of lists - singly and doubly, which both can hold any type of value (due to generics). List items are linked by utilizing a feature of structs that they can point to themselves. Main difference between them is that singly points only to the one side of a chain, whereas doubly is chained from the both sides so that it can be traversed through from both sides.

I also have introduced myself to Go's Unit testing. So Unit tests in this project serve not only as tests in general, but as well as a documentation that demonstrates how to use these structures.

## Features

1. Singly and doubly lists allow any type of value to be held.
1. Singly and doubly list can be traversed and node values easily retrieved.
1. Singly elements can be deleted from both sides (front and back).
1. Doubly lists have methods to add item to the front or to the back side of a list, as well as traversed in both directions.

## Considerations

* These linked lists could be way better constructed than I've made them here. Nodes could be separated from list itself to make a tracking easier;
* There could be definitely more functions to deal with them;
* Some improvement might be reasonable in terms of avoiding DRY violation (now there is some repetition amongs those 2 list types);
* Unit tests can be improved in many ways, especially by adding more test cases, probably by doing some refactoring or by reusing things and generalizing types via interfaces.