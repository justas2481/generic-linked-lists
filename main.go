package main

import (
	"fmt"
	"structures/lists"
)

func main() {
	data := lists.NewSingly[int]()
	data = data.Add(21)
	data = data.Add(22)
	data = data.Add(23)
	data = data.Add(24)
	data = data.DelFront().DelFront()
	data = data.Add(30)
	data = data.Add(40)
	data = data.Add(50)
	data = data.DelBack().DelBack()
	ok := true
	for {
		if !ok {
			break
		}
		fmt.Println("Data:", data.Val)
		data, ok = data.Forward()
	}

	doubly := lists.NewDoubly[int]()
	doubly = doubly.AddFront(66)
	doubly = doubly.AddFront(77)
	fmt.Println(doubly)

	d, ok := doubly.Forward()
	fmt.Println(d, ok)
}
