// Package lists defines well known two formws of linked lists,
// which are called singly and doubly lists.
package lists

// Interface to reflect that method which use this interface as a type,
// accepts any type.
// So this interface is basically used to restrict types of the values
// that linked lists can hold.
type constr interface {
	any
}

// Node is a type of any value allowed by constr interface.
type node constr

// Singly linked list is a structure that has a pointer to itself
// and generally points to a next instance.
// It forms a chain of values which are held by the pointer to the next element.
//
// Singly can hold any value as described by Node type.
// It implements node type as type parameter by following generics
// to allow any type to be hold as value within singly.
type singly[T node] struct {
	// Pointer which points to the next instance of a singly.
	next *singly[T]

	// Value that singly holds.
	Val T
}

// Creates empty singly and returns a pointer to its first item.
func NewSingly[T constr]() (item *singly[T]) {
	// Zero value of a type given by generics.
	var zeroth T

	// Return empty singly.
	return &singly[T]{next: nil, Val: zeroth}
}

// Adds new element to the beginning of a singly.
//
// Returns a pointer to the newly attached item.
func (l *singly[T]) Add(data T) (item *singly[T]) {
	return &singly[T]{next: l, Val: data}
}

// Moves forward by one element of a singly.
//
// Returns ok as true to indicate success, and false on failure.
//
// currentItem reflects a pointer to the next item within singly.
// If ok indicates failure, currentItem will have nil value.
func (s *singly[T]) Forward() (currentItem *singly[T], ok bool) {
	if s.next == nil || s.next.next == nil { // No more items
		return nil, false
	}

	return s.next, true
}

// Moves to the last element of a singly and returns a pointer to it.
func (s *singly[T]) Last() (lastItem *singly[T]) {
	// Points to the current instance.
	current := s
	var ok bool
	for {
		// Move by one pointer to the next element within the chain.
		current, ok = current.Forward()
		if !ok {
			break
		}

		s = current
	}

	return s
}

// Removes first element within singly
// and returns a pointer to the item which was next to the removed item.
func (s *singly[T]) DelFront() (item *singly[T]) {
	// Check if there is something to be removed.
	if s.next == nil || s.next.next == nil {
		// No more items to delete or this was the last item.
		return nil
	}

	s = s.next

	return s
}

// Removes last element within singly and returns a pointer to a receiver
// on success, or nil if there is nothing to be removed.
func (s *singly[T]) DelBack() (lastItem *singly[T]) {
	// Check if there is something to be removed.
	if s.next == nil || s.next.next == nil {
		// No more items to delete or this was the last item.
		return nil
	}

	temp, last := s, s
	for temp.next.next != nil {
		last = temp
		temp = temp.next
	}

	// Remove last item found.
	last.next = temp.next

	return s
}

// Doubly linked list is a structure that
// generally points to a previous and next instances of itself.
// It forms a chain of values which are held by the pointers from both sides
// like people in a row would hold hands together.
//
// Main benefit against singly list
// is that doubly list can be traversed bidirectionally.
//
// Doubly can hold any value as described by Node type.
// It implements node type as type parameter by following generics
// to allow any type to be hold as value within doubly.
type doubly[T node] struct {
	// Previous instance of an item within doubly.
	prev *doubly[T]

	// Next instance of an item within doubly.
	next *doubly[T]

	// Value of an item.
	Val T
}

// Creates empty doubly and returns a pointer to its first item.
func NewDoubly[T constr]() (item *doubly[T]) {
	// Zero value of a type given by generics.
	var zeroth T

	// Returns a pointer to the doubly item which has empty value
	// and points to nowhere from both sides.
	return &doubly[T]{prev: nil, next: nil, Val: zeroth}
}

// Adds new element to the beginning of a doubly chain.
//
// Returns a pointer to the newly attached item.
func (d *doubly[T]) AddFront(data T) (item *doubly[T]) {
	elem := NewDoubly[T]()
	first := d.First()
	elem.Val = data
	elem.prev = nil
	elem.next = first
	first.prev = elem

	return elem
}

// Adds new element to the end of a doubly chain.
//
// Returns a pointer to the newly attached item.
func (d *doubly[T]) AddBack(data T) (item *doubly[T]) {
	elem := NewDoubly[T]()
	last := d.Last()
	elem.Val = data
	elem.prev = last
	elem.next = nil
	last.next = elem

	return elem
}

// Moves forward by one element of a doubly.
//
// Returns ok as true to indicate success, and false on failure.
//
// currentItem reflects a pointer to the next item within doubly.
// If ok indicates failure, currentItem will have nil value.
func (d *doubly[T]) Forward() (currentItem *doubly[T], ok bool) {
	if d.next == nil || d.next.next == nil { // No next item
		return nil, false
	}

	return d.next, true
}

// Moves backward by one element of a doubly.
//
// Returns ok as true to indicate success, and false on failure.
//
// currentItem reflects a pointer to the previous item within doubly.
// If ok indicates failure, currentItem will have nil value.
func (d *doubly[T]) Backward() (currentItem *doubly[T], ok bool) {
	if d.prev == nil || d.prev.prev == nil { // No previous item
		return nil, false
	}

	return d.prev, true
}

// Moves to the first element of a doubly and returns a pointer to it.
func (d *doubly[T]) First() (item *doubly[T]) {
	// Points to the current instance.
	current := d
	var ok bool
	for {
		// Move by one pointer to the previous element within the chain.
		current, ok = current.Backward()
		if !ok {
			break
		}

		d = current
	}

	return d
}

// Moves to the last element of a doubly and returns a pointer to it.
func (d *doubly[T]) Last() (item *doubly[T]) {
	// Points to the current instance.
	current := d
	var ok bool
	for {
		// Move by one pointer to the next element within the chain.
		current, ok = current.Forward()
		if !ok {
			break
		}

		d = current
	}

	return d
}
