// Package lists defines well known two formws of linked lists,
// which are called singly and doubly lists.
package lists

import (
	"reflect"
	"testing"
)

// Creating singly.
func TestNewSingly(t *testing.T) {
	// Zero int value.
	var zerothInt int

	// Zero float slice value.
	var zerothFloat []float64

	// Test with integer values.
	testsInt := []struct {
		name     string
		wantItem *singly[int]
	}{
		{"Creating singly of ints", &singly[int]{next: nil, Val: zerothInt}},
	}
	for _, tt := range testsInt {
		t.Run(tt.name, func(t *testing.T) {
			if gotItem := NewSingly[int](); !reflect.DeepEqual(gotItem, tt.wantItem) {
				t.Errorf("NewSingly() = %v, want %v", gotItem, tt.wantItem)
			}
		})
	}

	// Test with float64 slices.
	testsFloats := []struct {
		name     string
		wantItem *singly[[]float64]
	}{
		{
			"Creating singly of float slices",
			&singly[[]float64]{next: nil, Val: zerothFloat},
		},
	}
	for _, tt := range testsFloats {
		t.Run(tt.name, func(t *testing.T) {
			if gotItem := NewSingly[[]float64](); !reflect.DeepEqual(gotItem, tt.wantItem) {
				t.Errorf("NewSingly() = %v, want %v", gotItem, tt.wantItem)
			}
		})
	}
}

// Add new item to a singly.
func TestAdd(t *testing.T) {
	singly := NewSingly[int]()

	t.Run(
		"Checking if new singly item has next pointer pointing to previous item",
		func(t *testing.T) {
			item := singly.Add(1)
			item2 := item.Add(2)

			// New item's next should point to singly's previous item.
			if item.next != singly {
				t.Errorf(
					"(*singly[t]).Add = %v, want %v",
					item.next,
					singly,
				)
			}
			if item2.next != item {
				t.Errorf(
					"(*singly[t]).Add = %v, want %v",
					item2.next,
					item,
				)
			}
		},
	)
}

// Moving forward by one element within the singly.
func TestForward(t *testing.T) {
	// Test if adding no elements,
	// moving towards next element should return ok as false.
	singly := NewSingly[int]()
	t.Run(
		"Traversing empty singly and singly with 1 item",
		func(t *testing.T) {
			if item, ok := singly.Forward(); ok || item != nil {
				t.Error(
					"(*singly[t]).Forward, does not return ok as false when singly is empty",
				)
			}
			// Add single item.
			item := singly.Add(1)
			if item, ok := item.Forward(); ok || item != nil {
				t.Error(
					"(*singly[t]).Forward, does not return ok as false when singly has one item",
				)
			}
			// Do not leave out first item in a singly.
			singly = item
		},
	)

	// Add 2 aditional items to the singly.
	item2 := singly.Add(2)
	item3 := item2.Add(3)
	t.Run(
		"Traversing through singly items",
		func(t *testing.T) {
			// Go from item3 to item1 through the chain.
			current := item3
			for {
				next, ok := current.Forward()
				// Check if Forward method gives next pointer.
				if ok && current.next != next {
					t.Errorf(
						"(*singly[t]).Forward = %v, want %v",
						next,
						current.next,
					)
				}
				current = next
				// If not ok, then there is a last pointer (nil).
				if !ok {
					break
				}

			}

		},
	)
}

// Move to the last item in singly.
func TestLast(t *testing.T) {
	// Make a simple list of 3 items.
	singly := NewSingly[int]()
	item1 := singly.Add(1)
	item2 := item1.Add(2)
	item3 := item2.Add(2)

	t.Run("Moving to the last singly item", func(t *testing.T) {
		// We want that Last() method would move to the first item in a chain.
		want := item1
		got := item3.Last()
		if got != want {
			t.Errorf("(*singly[t]).Last() = %v, %v", got, want)
		}
	})
}

// Creating new doubly.
func TestNewDoubly(t *testing.T) {
	// Zero int value.
	var zerothInt int

	// Zero float slice value.
	var zerothFloat []float64

	// Test with integer values.
	testsInt := []struct {
		name     string
		wantItem *doubly[int]
	}{
		{"Creating doubly of ints", &doubly[int]{prev: nil, next: nil, Val: zerothInt}},
	}
	for _, tt := range testsInt {
		t.Run(tt.name, func(t *testing.T) {
			if gotItem := NewDoubly[int](); !reflect.DeepEqual(gotItem, tt.wantItem) {
				t.Errorf("NewDoubly() = %v, want %v", gotItem, tt.wantItem)
			}
		})
	}

	// Test with float64 slices.
	testsFloats := []struct {
		name     string
		wantItem *doubly[[]float64]
	}{
		{
			"Creating doubly of float slices",
			&doubly[[]float64]{prev: nil, next: nil, Val: zerothFloat},
		},
	}
	for _, tt := range testsFloats {
		t.Run(tt.name, func(t *testing.T) {
			if gotItem := NewDoubly[[]float64](); !reflect.DeepEqual(gotItem, tt.wantItem) {
				t.Errorf("NewDoubly() = %v, want %v", gotItem, tt.wantItem)
			}
		})
	}
}

// Adding new item to the front of a doubly.
//
// We do not test First(), because it's called here.
func TestDoublyAddFront(t *testing.T) {
	doubly := NewDoubly[int]()

	t.Run(
		"Checking if new doubly item has next pointer pointing to right item",
		func(t *testing.T) {
			item := doubly.AddFront(1)
			item2 := item.AddFront(2)

			// New item's next should point to doubly's previous item
			// and prev item should hold new item as well.
			if item.next != doubly || doubly.prev != item {
				t.Errorf(
					"(*doubly[t]).AddFront = %v, want %v",
					item.next,
					doubly,
				)
			}
			// Item2 should hold item as next and item should hold item2 as it's prev.
			if item2.next != item || item.prev != item2 {
				t.Errorf(
					"(*doubly[t]).AddFront = %v, want %v",
					item2.next,
					item,
				)
			}
		},
	)
}

// Adding new item to the end of a doubly.
//
// We do not test Last(), because it's called here.
func TestDoublyAddBack(t *testing.T) {
	doubly := NewDoubly[int]()

	t.Run(
		"Checking if new doubly item has prev pointer pointing to right item",
		func(t *testing.T) {
			item := doubly.AddBack(1)
			item2 := item.AddBack(2)

			// New item's prev should point to doubly's previous item
			// and prev item should hold new item on next as well.
			if item.prev != doubly || doubly.next != item {
				t.Errorf(
					"(*doubly[t]).AddBack = %v, want %v",
					item.prev,
					doubly,
				)
			}
			// Item2 should hold item as prev and item should hold item2 as it's next.
			if item2.prev != item || item.next != item2 {
				t.Errorf(
					"(*doubly[t]).AddBack = %v, want %v",
					item2.prev,
					item,
				)
			}
		},
	)
}

// Remove item in front of a singly.
func TestSinglyDelFront(t *testing.T) {
	singly := NewSingly[int]()
	// Try to remove something from an empty singly.
	if singly.DelFront() != nil {
		t.Error("There is impossible to delete something from an empty singly")
	}

	item := singly.Add(1)
	// Remove 1 and only item.
	if item.DelFront() != nil {
		t.Error(
			"when in singly there is only 1 item, after removing it, result should be nil",
		)
	}

	item2 := item.Add(2)
	item3 := item2.Add(3)
	// Remove first / newest item, so expect item4 to hold item2, not item3.
	item4 := item3.DelFront()
	if item4 != item2 {
		t.Error("Item wasn't removed from the front of a singly")
	}
}

// Remove last item in a singly.
func TestSinglyDelBack(t *testing.T) {
	singly := NewSingly[int]()
	// Try to remove something from an empty singly.
	if singly.DelBack() != nil {
		t.Error("There is impossible to delete something from an empty singly")
	}

	item := singly.Add(1)
	// Remove 1 and only item.
	if item.DelBack() != nil {
		t.Error(
			"when in singly there is only 1 item, after removing it, result should be nil",
		)
	}

	item2 := item.Add(2)
	item3 := item2.Add(3)
	// Remove last / oldest item, so expect item4 to hold item2, not item.
	item4 := item3.DelBack()
	if item4.Last() != item2 {
		t.Error("Item wasn't removed from the back of a singly")
	}
}
